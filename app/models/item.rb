class Item < ApplicationRecord
    
    include ItemHelper
    
    attr_accessor :asin,:name,:image_url,:price,:score,:reviews,:rank,:detail
    
    #ids : ASIN
    #name : Small/title
    #image_url : Images LargeImages/URL
    #price : Offers
    #score : Reviews & Nokogiri
    #reviews : Reviews/ IFrameUrl
    #rank : Item/SalesRank
    
    def initialize(asin,name,image_url,price,rank,reviews,detail)
        @asin = asin
        @image_url = image_url
        @name = name
        @price = price
        @rank = rank
        @reviews = reviews
        @detail = detail
    end
end
