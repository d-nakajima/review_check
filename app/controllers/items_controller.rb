class ItemsController < ApplicationController

include ItemsHelper

def search 
  
  if params[:keyword].present? 
    Amazon::Ecs.debug = true
    
    items_info = get_item_info(params[:keyword])
    
    if items_info.present?
      @items = apply_model(items_info)
      @item = get_popular
      
       if @item.price.nil?
          @item.price = "価格情報なし"
       end
       
    end
    
   
    
    
  end
end

  def show
  end
  
  
end