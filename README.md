
androidで商品固有のISBNやJANコード（BarCode）を読み取ってamazonのレビューを表示するアプリのweb部分です。

heroku上にアップロードしてあります。

=> https://checkreview.herokuapp.com/?keyword=ruby  



引数keywordでamazonを検索し、その中で最も売れている商品のレビューを表示します。
androidからwebviewでurlを開いて、keywordにBarCodeの情報を付加しています。



cloud9環境下でruby on rails,amazon-ecsなどを利用して作成しました。
かねてから作りたいwebアプリケーションがQRコードとwebからの情報取得が必要だったので、その練習も兼ねて作りました。



amazon-ecsそのものに制限があり、(reviewそのものの取得はできず、reviewをiframeで表示するためのURLを取得する)、若干不完全燃焼ですが、練習にはなりました。