Rails.application.routes.draw do
  get '/show',to: 'items#show'
  root 'items#search'
  get '/search/:id' => 'items#search'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
